package org.example.krit.kplayerinfo.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryHolder;
import org.example.krit.kplayerinfo.menusystem.Menu;

public class InventoryMoveListener implements Listener {
    @EventHandler
    public void onMenuClick(InventoryClickEvent e) {

        InventoryHolder holder = e.getInventory().getHolder();

        if (holder instanceof Menu) {
            e.setCancelled(true);

            if (e.getCurrentItem() == null) {
                return;
            }

            ((Menu) holder).handleMenu(e);
        }

    }

}
