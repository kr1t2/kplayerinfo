package org.example.krit.kplayerinfo.utils;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Utils {

    public static LinkedHashMap<UUID, Integer> sortHashMap(HashMap<UUID, Integer> hashMap) {

        return hashMap.entrySet()
                .stream()
                .sorted(Map.Entry.<UUID, Integer>comparingByValue().reversed())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }


    public static String millisecToTime(long milliseconds) {
        final long day = TimeUnit.MILLISECONDS.toDays(milliseconds);

        final long hours = TimeUnit.MILLISECONDS.toHours(milliseconds)
                - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(milliseconds));

        final long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds)
                - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliseconds));

        final long seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds)
                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds));

        if (seconds == 0 && minutes == 0 && hours == 0 && day == 0) {
            return "Только что";
        } else if (minutes == 0 && hours == 0 && day == 0) {
            return String.format("%d сек.", seconds);
        } else if (hours == 0 && day == 0) {
            return String.format("%d мин. %d сек.", minutes, seconds);
        } else if (day == 0) {
            return String.format("%d ч. %d мин. %d сек.", hours, minutes, seconds);
        } else {
            return String.format("%d д. %d ч. %d мин. %d сек.", day, hours, minutes, seconds);
        }
    }

    public static void sendClickableURL(Player player, String message, String link) {

        net.md_5.bungee.api.chat.TextComponent component = new TextComponent(net.md_5.bungee.api.chat.TextComponent.fromLegacyText(message));
        // Add a click event to the component.
        component.setClickEvent(new net.md_5.bungee.api.chat.ClickEvent(ClickEvent.Action.OPEN_URL, link));

        // Send it
        player.spigot().sendMessage(component);


    }
}
