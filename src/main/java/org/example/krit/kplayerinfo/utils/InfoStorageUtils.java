package org.example.krit.kplayerinfo.utils;

import com.google.gson.Gson;
import org.example.krit.kplayerinfo.KPlayerInfo;
import org.example.krit.kplayerinfo.model.Info;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InfoStorageUtils {

    private static ArrayList<Info> informations = new ArrayList<Info>();

    public static Info createInfo(String playerName) {

        Info info = new Info(playerName, null, null, null, null, null, null, null);
        informations.add(info);

        return info;
    }

    public static void deleteInfo(String playerName) throws IOException {
        for (Info info : informations) {
            if (info.getPlayerName().equalsIgnoreCase(playerName)) {
                informations.remove(info);
                break;
            }
        }
        saveInfo();
    }

    public static Info findInfo(String playerName) {
        for (Info info : informations) {
            if (info.getPlayerName().equalsIgnoreCase(playerName)) {
                return info;
            }
        }
        return null;
    }

    public static Info updateInfo(String playerName, Info newInfo) throws IOException {
        for (Info info : informations) {
            if (info.getPlayerName().equalsIgnoreCase(playerName)) {
                info.setBaseLocation(newInfo.getBaseLocation());
                info.setClan(newInfo.getClan());
                info.setOtherInfo(newInfo.getOtherInfo());
                info.setBuildings(newInfo.getBuildings());
                info.setDiscord(newInfo.getDiscord());
                info.setYoutube(newInfo.getYoutube());
                info.setTwitch(newInfo.getTwitch());
                info.setPlayerName(newInfo.getPlayerName());
                info.setPlayerName(newInfo.getPlayerName());

                saveInfo();
                break;
            }
        }
        return null;
    }

    public static List<Info> findAllInformations() {
        return informations;
    }

    public static void loadInfo() throws IOException {

        Gson gson = new Gson();
        File file = new File(KPlayerInfo.getInstance().getDataFolder().getAbsolutePath() + "/playerinfo.json");
        if (file.exists()) {
            Reader reader = new FileReader(file);
            Info[] n = gson.fromJson(reader, Info[].class);
            informations = new ArrayList<>(Arrays.asList(n));
        }

    }

    public static void saveInfo() throws IOException {

        Gson gson = new Gson();
        File file = new File(KPlayerInfo.getInstance().getDataFolder().getAbsolutePath() + "/playerinfo.json");
        file.getParentFile().mkdir();
        file.createNewFile();
        Writer writer = new FileWriter(file, false);
        gson.toJson(informations, writer);
        writer.flush();
        writer.close();

    }

}