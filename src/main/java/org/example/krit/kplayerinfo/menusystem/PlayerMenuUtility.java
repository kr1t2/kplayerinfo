package org.example.krit.kplayerinfo.menusystem;

import org.bukkit.entity.Player;

public class PlayerMenuUtility {

    private Player owner;
    private Player playerTarget;

    public PlayerMenuUtility(Player p) {
        this.owner = p;
    }

    public Player getOwner() {
        return owner;
    }

    public Player getPlayerTarget() {
        return playerTarget;
    }

    public void setPlayerTarget(Player playerTarget) {
        this.playerTarget = playerTarget;
    }
}
