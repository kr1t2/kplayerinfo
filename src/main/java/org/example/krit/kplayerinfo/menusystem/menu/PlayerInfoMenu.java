package org.example.krit.kplayerinfo.menusystem.menu;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.example.krit.kplayerinfo.KPlayerInfo;
import org.example.krit.kplayerinfo.menusystem.Menu;
import org.example.krit.kplayerinfo.menusystem.PlayerMenuUtility;
import org.example.krit.kplayerinfo.model.Info;
import org.example.krit.kplayerinfo.utils.InfoStorageUtils;
import org.example.krit.kplayerinfo.utils.Utils;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class PlayerInfoMenu extends Menu {
    private final OfflinePlayer p;
    private final Info info;

    public PlayerInfoMenu(PlayerMenuUtility playerMenuUtility, OfflinePlayer p) {
        super(playerMenuUtility);
        this.p = p;

        if (InfoStorageUtils.findInfo(p.getName()) == null) {
            InfoStorageUtils.createInfo(p.getName());
        }
        this.info = InfoStorageUtils.findInfo(p.getName());
    }

    @Override
    public String getMenuName() {
        return "Информация о игроке " + p.getName();
    }

    @Override
    public int getSlots() {
        return 54;
    }

    @Override
    public void handleMenu(InventoryClickEvent e) {

        switch (e.getCurrentItem().getType()) {
            case RED_BANNER:

                if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getLore().get(0)).contains("Информация не указана игроком")) {
                    return;
                }

                inventory.close();
                String youtubeLink;

                if (ChatColor.stripColor(e.getCurrentItem().getLore().get(0)).contains("https://www.youtube.com/@")) {
                    youtubeLink = ChatColor.stripColor(e.getCurrentItem().getLore().get(0));
                } else if (ChatColor.stripColor(e.getCurrentItem().getLore().get(0)).charAt(0) == '@') {
                    youtubeLink = "https://www.youtube.com/" + ChatColor.stripColor(e.getCurrentItem().getLore().get(0));
                } else {
                    youtubeLink = "https://www.youtube.com/@" + ChatColor.stripColor(e.getCurrentItem().getLore().get(0));
                }

                String youtubeMessage = ChatColor.translateAlternateColorCodes('&', KPlayerInfo.getInstance().getConfig().getString("clickable-youtube-url"))
                        .replaceAll("<link>", youtubeLink)
                        .replaceAll("<player>", e.getWhoClicked().getName());

                Utils.sendClickableURL((Player) e.getWhoClicked(), youtubeMessage, youtubeLink);

                break;
            case CYAN_BANNER:

                if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getLore().get(0)).contains("Информация не указана игроком")) {
                    return;
                }

                inventory.close();
                String twitchLink;

                if (ChatColor.stripColor(e.getCurrentItem().getLore().get(0)).contains("https://www.twitch.tv/")) {
                    twitchLink = ChatColor.stripColor(e.getCurrentItem().getLore().get(0));
                } else {
                    twitchLink = "https://www.twitch.tv/" + ChatColor.stripColor(e.getCurrentItem().getLore().get(0));
                }

                String twitchMessage = ChatColor.translateAlternateColorCodes('&', KPlayerInfo.getInstance().getConfig().getString("clickable-twitch-url"))
                        .replaceAll("<link>", twitchLink)
                        .replaceAll("<player>", e.getWhoClicked().getName());

                Utils.sendClickableURL((Player) e.getWhoClicked(), twitchMessage, twitchLink);

                break;

        }
    }

    @Override
    public void setMenuItems() {
        setFillerGlass();

        inventory.setItem(4, this.getHeadItem());

        inventory.setItem(11, this.getLastPlayedItem());
        inventory.setItem(12, this.getFirstPlayedItem());
        inventory.setItem(14, this.getTotalWorldTimeItem());
        inventory.setItem(15, this.getIsBannedItem());

        inventory.setItem(29, this.getBaseLocationItem());
        inventory.setItem(30, this.getClanInfoItem());
        inventory.setItem(32, this.getPlayerBuildingsItem());
        inventory.setItem(33, this.getOtherInfoItem());

        inventory.setItem(53, this.getDiscordItem());
        inventory.setItem(45, this.getYoutubeItem());
        inventory.setItem(46, this.getTwitchItem());

    }


    private @NotNull ItemStack getHeadItem() {
        SkullMeta skull = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.PLAYER_HEAD);
        skull.setOwningPlayer(this.p);
        skull.setDisplayName("Информация о " + ChatColor.BLUE + p.getName());

        ItemStack item = new ItemStack(Material.PLAYER_HEAD);
        item.setItemMeta(skull);


        return item;
    }

    private @NotNull ItemStack getLastPlayedItem() {
        ItemStack item = new ItemStack(Material.CLOCK);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(ChatColor.BLUE + "Заходил последний раз:");

        if (p.isOnline()) {

            itemMeta.setLore(List.of(ChatColor.GOLD + "Игрок сейчас в сети"));

        } else {
            long timeNow = System.currentTimeMillis();
            long lastPlayedTime = p.getLastLogin();

            String timePassed = Utils.millisecToTime(timeNow - lastPlayedTime);

            itemMeta.setLore(List.of(ChatColor.GOLD + timePassed + " назад"));
        }

        item.setItemMeta(itemMeta);
        return item;
    }

    private @NotNull ItemStack getFirstPlayedItem() {
        ItemStack item = new ItemStack(Material.OAK_DOOR);
        ItemMeta itemMeta = item.getItemMeta();


        long timeNow = System.currentTimeMillis();
        long firstPlayed = p.getFirstPlayed();

        String timePassed = Utils.millisecToTime(timeNow - firstPlayed);

        itemMeta.setDisplayName(ChatColor.BLUE + "Зашёл впервые:");
        itemMeta.setLore(List.of(ChatColor.GOLD + timePassed + " назад"));


        item.setItemMeta(itemMeta);
        return item;
    }

    private @NotNull ItemStack getIsBannedItem() {
        ItemStack item = new ItemStack(p.isBanned() ? Material.BARRIER : Material.DIAMOND);
        ItemMeta itemMeta = item.getItemMeta();

        if (p.isBanned()) {
            itemMeta.setDisplayName(ChatColor.RED + "Игрок забанен");
        } else {
            itemMeta.setDisplayName(ChatColor.GREEN + "Игрок не в бане");
        }

        item.setItemMeta(itemMeta);
        return item;
    }

    private ItemStack getTotalWorldTimeItem() {
        ItemStack item = new ItemStack(Material.COMPASS);
        ItemMeta itemMeta = item.getItemMeta();

        String playerTime = Utils.millisecToTime(p.getStatistic(Statistic.TOTAL_WORLD_TIME) * 50L);

        itemMeta.setDisplayName(ChatColor.BLUE + "Наиграно на сервере:");

        HashMap<UUID, Integer> playersTime = new HashMap<>();
        for (OfflinePlayer people : Bukkit.getOfflinePlayers()) {
            playersTime.put(people.getUniqueId(), people.getStatistic(Statistic.TOTAL_WORLD_TIME));
        }
        LinkedHashMap<UUID, Integer> sortedStat = Utils.sortHashMap(playersTime);

        int place = 1;
        for (Map.Entry<UUID, Integer> entry : sortedStat.entrySet()) {
            if (p.getUniqueId() == entry.getKey()) break;
            place++;
        }

        itemMeta.setLore(List.of(ChatColor.GOLD + playerTime, ChatColor.YELLOW + String.valueOf(place) + " Место в топе"));

        item.setItemMeta(itemMeta);

        return item;
    }

    private ItemStack getBaseLocationItem() {
        ItemStack item = new ItemStack(Material.CHEST);
        ItemMeta itemMeta = item.getItemMeta();

        itemMeta.setDisplayName(ChatColor.BLUE + "Информация о базе игрока");

        String info = this.info.getBaseLocation();

        if (info != null) {

            ArrayList<String> infoList = new ArrayList<>();
            StringBuilder tempAppendString = new StringBuilder();

            for (int i = 0; i < info.length(); i++) {
                if (i % 35 == 0 && i >= 35) {
                    tempAppendString.append(info.charAt(i) + '-');
                    infoList.add(ChatColor.WHITE + tempAppendString.toString());
                    tempAppendString = new StringBuilder();
                } else {
                    tempAppendString.append(info.charAt(i));
                }
            }
            infoList.add(ChatColor.WHITE + tempAppendString.toString());

            itemMeta.setLore(infoList);
        } else {
            itemMeta.setLore(List.of(ChatColor.WHITE + "Информация не указана игроком", ChatColor.WHITE + "Чтобы указать напишите " + ChatColor.GOLD + "/setInfo baseLocation <info>"));
        }

        item.setItemMeta(itemMeta);
        return item;
    }

    private ItemStack getClanInfoItem() {
        ItemStack item = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta itemMeta = item.getItemMeta();

        itemMeta.setDisplayName(ChatColor.BLUE + "Информация о клане игрока");

        String info = this.info.getClan();

        if (info != null) {

            ArrayList<String> infoList = new ArrayList<>();
            StringBuilder tempAppendString = new StringBuilder();

            for (int i = 0; i < info.length(); i++) {
                if (i % 35 == 0 && i >= 35) {
                    tempAppendString.append(info.charAt(i) + '-');
                    infoList.add(ChatColor.WHITE + tempAppendString.toString());
                    tempAppendString = new StringBuilder();
                } else {
                    tempAppendString.append(info.charAt(i));
                }
            }
            infoList.add(ChatColor.WHITE + tempAppendString.toString());

            itemMeta.setLore(infoList);
        } else {
            itemMeta.setLore(List.of(ChatColor.WHITE + "Информация не указана игроком", ChatColor.WHITE + "Чтобы указать напишите " + ChatColor.GOLD + "/setInfo clan <info>"));
        }

        item.setItemMeta(itemMeta);
        return item;
    }

    private ItemStack getPlayerBuildingsItem() {
        ItemStack item = new ItemStack(Material.BRICKS);
        ItemMeta itemMeta = item.getItemMeta();

        itemMeta.setDisplayName(ChatColor.BLUE + "Информация о постройках игрока");

        String info = this.info.getBuildings();

        if (info != null) {

            ArrayList<String> infoList = new ArrayList<>();
            StringBuilder tempAppendString = new StringBuilder();

            for (int i = 0; i < info.length(); i++) {
                if (i % 35 == 0 && i >= 35) {
                    tempAppendString.append(info.charAt(i) + '-');
                    infoList.add(ChatColor.WHITE + tempAppendString.toString());
                    tempAppendString = new StringBuilder();
                } else {
                    tempAppendString.append(info.charAt(i));
                }
            }
            infoList.add(ChatColor.WHITE + tempAppendString.toString());
            itemMeta.setLore(infoList);
        } else {
            itemMeta.setLore(List.of(ChatColor.WHITE + "Информация не указана игроком", ChatColor.WHITE + "Чтобы указать напишите " + ChatColor.GOLD + "/setInfo buildings <info>"));
        }

        item.setItemMeta(itemMeta);
        return item;
    }

    private ItemStack getOtherInfoItem() {
        ItemStack item = new ItemStack(Material.PAPER);
        ItemMeta itemMeta = item.getItemMeta();

        itemMeta.setDisplayName(ChatColor.BLUE + "Остальная информация игрока");

        String info = this.info.getOtherInfo();

        if (info != null) {

            ArrayList<String> infoList = new ArrayList<>();
            StringBuilder tempAppendString = new StringBuilder();

            for (int i = 0; i < info.length(); i++) {
                if (i % 35 == 0 && i >= 35) {
                    tempAppendString.append(info.charAt(i)).append("-");
                    infoList.add(ChatColor.WHITE + tempAppendString.toString());
                    tempAppendString = new StringBuilder();
                } else {
                    tempAppendString.append(info.charAt(i));
                }
            }
            infoList.add(ChatColor.WHITE + tempAppendString.toString());

            itemMeta.setLore(infoList);
        } else {
            itemMeta.setLore(List.of(ChatColor.WHITE + "Информация не указана игроком", ChatColor.WHITE + "Чтобы указать напишите " + ChatColor.GOLD + "/setInfo otherInfo <info>"));
        }

        item.setItemMeta(itemMeta);
        return item;
    }

    private ItemStack getDiscordItem() {
        ItemStack item = new ItemStack(Material.BLUE_BANNER);
        ItemMeta itemMeta = item.getItemMeta();

        itemMeta.setDisplayName(ChatColor.BLUE + "Discord игрока");

        String discord = info.getDiscord();
        if (discord == null) {
            itemMeta.setLore(List.of(ChatColor.WHITE + "Информация не указана игроком", ChatColor.WHITE + "Чтобы указать напишите " + ChatColor.GOLD + "/setInfo discord <discordTag>"));
        } else {
            itemMeta.setLore(List.of(ChatColor.GOLD + discord));
        }

        item.setItemMeta(itemMeta);
        return item;
    }

    private ItemStack getYoutubeItem() {
        ItemStack item = new ItemStack(Material.RED_BANNER);
        ItemMeta itemMeta = item.getItemMeta();

        itemMeta.setDisplayName(ChatColor.BLUE + "Youtube игрока");

        String youtube = info.getYoutube();
        if (youtube == null) {
            itemMeta.setLore(List.of(ChatColor.WHITE + "Информация не указана игроком", ChatColor.WHITE + "Чтобы указать напишите " + ChatColor.GOLD + "/setInfo youtube <discordTag>"));
        } else {
            itemMeta.setLore(List.of(ChatColor.GOLD + youtube, ChatColor.UNDERLINE + "" + ChatColor.AQUA + "Нажмите чтобы перейти"));
        }

        item.setItemMeta(itemMeta);
        return item;
    }

    private ItemStack getTwitchItem() {
        ItemStack item = new ItemStack(Material.CYAN_BANNER);
        ItemMeta itemMeta = item.getItemMeta();

        itemMeta.setDisplayName(ChatColor.BLUE + "Twitch игрока");

        String twitch = info.getTwitch();
        if (twitch == null) {
            itemMeta.setLore(List.of(ChatColor.WHITE + "Информация не указана игроком", ChatColor.WHITE + "Чтобы указать напишите " + ChatColor.GOLD + "/setInfo twitch <discordTag>"));
        } else {
            itemMeta.setLore(List.of(ChatColor.GOLD + twitch, ChatColor.UNDERLINE + "" + ChatColor.AQUA + "Нажмите чтобы перейти"));
        }

        item.setItemMeta(itemMeta);
        return item;
    }
}
