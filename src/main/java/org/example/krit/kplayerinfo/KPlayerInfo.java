package org.example.krit.kplayerinfo;

import com.palmergames.adventure.platform.bukkit.BukkitAudiences;
import org.bukkit.plugin.java.JavaPlugin;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.example.krit.kplayerinfo.commands.KPlayerInfoCommand;
import org.example.krit.kplayerinfo.commands.PlayerInfoCommand;
import org.example.krit.kplayerinfo.commands.PlayerSetInfoCommand;
import org.example.krit.kplayerinfo.listeners.InventoryMoveListener;
import org.example.krit.kplayerinfo.utils.InfoStorageUtils;

import java.io.IOException;

public final class KPlayerInfo extends JavaPlugin {

    private static KPlayerInfo plugin;
    private BukkitAudiences adventure;

    @Override
    public void onEnable() {

        plugin = this;
        this.adventure = BukkitAudiences.create(this);

        getConfig().options().copyDefaults();
        saveDefaultConfig();

        // События
        getServer().getPluginManager().registerEvents(new InventoryMoveListener(), this);

        // Комманды
        getCommand("setInfo").setExecutor(new PlayerSetInfoCommand());
        getCommand("setInfo").setTabCompleter(new PlayerSetInfoCommand());

        getCommand("getInfo").setExecutor(new PlayerInfoCommand());
        getCommand("getInfo").setTabCompleter(new PlayerInfoCommand());

        getCommand("playerinfo").setExecutor(new KPlayerInfoCommand());
        getCommand("playerinfo").setTabCompleter(new KPlayerInfoCommand());

        // Загрузить информация из файла JSON
        try {
            InfoStorageUtils.loadInfo();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static KPlayerInfo getInstance() {
        return plugin;
    }

    public @NonNull BukkitAudiences adventure() {
        if (this.adventure == null) {
            throw new IllegalStateException("Tried to access Adventure when the plugin was disabled!");
        }
        return this.adventure;
    }
}
