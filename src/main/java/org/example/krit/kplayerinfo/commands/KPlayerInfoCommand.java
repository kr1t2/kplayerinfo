package org.example.krit.kplayerinfo.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.*;
import org.bukkit.entity.Player;
import org.example.krit.kplayerinfo.KPlayerInfo;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;

public class KPlayerInfoCommand implements CommandExecutor, TabCompleter {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if (args.length == 0) {
            return true;
        }
        switch (args[0]) {
            case "reload":
                if (sender instanceof Player p) {
                    if (!p.hasPermission("kplayerinfo.reload")) {
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KPlayerInfo.getInstance().getConfig().getString("no-permission-message"))));

                    } else {
                        KPlayerInfo.getInstance().saveConfig();
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KPlayerInfo.getInstance().getConfig().getString("no-permission-message"))));

                    }
                    return true;
                } else if (sender instanceof BlockCommandSender) {
                    return true;
                }
                KPlayerInfo.getInstance().saveConfig();
                Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KPlayerInfo.getInstance().getConfig().getString("no-permission-message"))));


                break;
            case "help":
                if (sender instanceof Player p) {

                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KPlayerInfo.getInstance().getConfig().getString("examples.get-info-command-example"))));
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KPlayerInfo.getInstance().getConfig().getString("examples.set-info-command-example"))));

                    return true;

                } else if (sender instanceof BlockCommandSender) {
                    return true;
                }
                Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KPlayerInfo.getInstance().getConfig().getString("examples.get-info-command-example"))));
                Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KPlayerInfo.getInstance().getConfig().getString("examples.set-info-command-example"))));

        }

        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {

        if (args.length == 1) {
            return List.of("reload", "help");
        }

        return null;
    }
}
