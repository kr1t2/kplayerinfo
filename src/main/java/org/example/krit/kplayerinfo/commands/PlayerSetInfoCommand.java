package org.example.krit.kplayerinfo.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.example.krit.kplayerinfo.KPlayerInfo;
import org.example.krit.kplayerinfo.model.Info;
import org.example.krit.kplayerinfo.utils.InfoStorageUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class PlayerSetInfoCommand implements CommandExecutor, TabCompleter {

    private final List<String> COMPLETERS = List.of("clan", "baseLocation", "buildings", "otherInfo", "discord", "youtube", "twitch");

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {

        if (sender instanceof Player p) {

            if (args.length < 1) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', KPlayerInfo.getInstance().getConfig().getString("no-args-message")));
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', KPlayerInfo.getInstance().getConfig().getString("examples.set-info-command-example")));

                return true;
            }

            boolean isArgsNormal = false;
            for (String str : COMPLETERS) {
                if (args[0].equalsIgnoreCase(str)) {
                    isArgsNormal = true;
                    break;
                }
            }
            args[0] = args[0].toLowerCase();

            if (!isArgsNormal) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KPlayerInfo.getInstance().getConfig().getString("wrong-args-message")).replaceAll("<wrongArg>", args[0])));
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KPlayerInfo.getInstance().getConfig().getString("examples.get-info-command-example"))));

                return true;
            }

            StringBuilder messageBuilder = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                messageBuilder.append(args[i]).append(" ");
            }
            String message = messageBuilder.toString().strip();

            Info info = InfoStorageUtils.findInfo(p.getName());
            if (info == null) {
                info = InfoStorageUtils.createInfo(p.getName());
            }

            if (message == null) {
                message = "\"\"";
            }

            if (message.contains(" ") && (args[0].equals("youtube") || args[0].equals("twitch") || args[0].equals("discord"))) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KPlayerInfo.getInstance().getConfig().getString("link-error-message"))));
                return true;
            }

            switch (args[0]) {
                case "clan" -> info.setClan(message);
                case "buildings" -> info.setBuildings(message);
                case "baselocation" -> info.setBaseLocation(message);
                case "otherinfo" -> info.setOtherInfo(message);
                case "youtube" -> info.setYoutube(message);
                case "discord" -> info.setDiscord(message);
                case "twitch" -> info.setTwitch(message);
            }


            try {
                InfoStorageUtils.updateInfo(p.getName(), info);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', KPlayerInfo.getInstance().getConfig().getString("successful-set-info").replaceAll("<key>", args[0]).replaceAll("<value>", message)));

        }

        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {

        if (args.length == 1) {
            return COMPLETERS;
        }

        return null;
    }
}
