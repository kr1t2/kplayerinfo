package org.example.krit.kplayerinfo.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import org.example.krit.kplayerinfo.KPlayerInfo;
import org.example.krit.kplayerinfo.menusystem.PlayerMenuUtility;
import org.example.krit.kplayerinfo.menusystem.menu.PlayerInfoMenu;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class PlayerInfoCommand implements CommandExecutor, TabCompleter {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {

        if (sender instanceof Player p) {
            OfflinePlayer target;
            if (args.length == 0) {
                target = p;
            } else {
                target = Bukkit.getOfflinePlayer(args[0]);
                if (!target.hasPlayedBefore()) {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', KPlayerInfo.getInstance().getConfig().getString("player-doesnt-exist-message")));
                    return true;
                }
            }
            new PlayerInfoMenu(new PlayerMenuUtility(p), target).open();
        }


        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {

        if (args.length == 1) {
            ArrayList<String> players = new ArrayList<>();

            for (OfflinePlayer people : Bukkit.getOfflinePlayers()) {
                players.add(people.getName());
            }

            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[0], players, completions);
            return completions;
        }

        return null;
    }
}
