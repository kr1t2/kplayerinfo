package org.example.krit.kplayerinfo.model;

public class Info {

    private String playerName;
    private String baseLocation;
    private String clan;
    private String buildings;
    private String otherInfo;
    private String discord;
    private String youtube;
    private String twitch;

    public Info(String playerName, String baseLocation, String clan, String buildings, String otherInfo, String discord, String youtube, String twitch) {
        this.playerName = playerName;
        this.baseLocation = baseLocation;
        this.clan = clan;
        this.buildings = buildings;
        this.otherInfo = otherInfo;
        this.discord = discord;
        this.youtube = youtube;
        this.twitch = twitch;
    }


    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getBaseLocation() {
        return baseLocation;
    }

    public void setBaseLocation(String baseLocation) {
        this.baseLocation = baseLocation;
    }

    public String getClan() {
        return clan;
    }

    public void setClan(String clan) {
        this.clan = clan;
    }

    public String getBuildings() {
        return buildings;
    }

    public void setBuildings(String buildings) {
        this.buildings = buildings;
    }

    public String getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    public String getDiscord() {
        return discord;
    }

    public void setDiscord(String discord) {
        this.discord = discord;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getTwitch() {
        return twitch;
    }

    public void setTwitch(String twitch) {
        this.twitch = twitch;
    }
}